window.cloudready = false;

var pc = new RTCPeerConnection({sdpSemantics:'unified-plan'});

// get DOM elements
//var dataChannelLog = document.getElementById('data-channel');
var screen = document.getElementById('screen');

// register some listeners to help debugging
pc.addEventListener('icegatheringstatechange', function() {
    console.log('ICE_GATHERING_STATE_CHANGE', pc.iceGatheringState);
}, false);

pc.addEventListener('iceconnectionstatechange', function() {
    console.log('ICE_CONNECTION_STATE_CHANGE', pc.iceConnectionState);
    if(pc.iceConnectionState == "disconnected") {
        window.cloudready = false;

        document.getElementById('disconnected').style.display = 'inline-block';
        screen.style.display = 'none';
    }
}, false);

pc.addEventListener('signalingstatechange', function() {
    console.log('ICE_SIGNAL_STATE_CHANGE', pc.signalingState);
}, false);

// connect audio / video
pc.addEventListener('track', function(evt) {
    if (evt.track.kind == 'video') {
        screen.srcObject = evt.streams[0];
        window.cloudready = true;
    } else {
        document.getElementById('audio').srcObject = evt.streams[0];
    }
});
// data channel
var dc = null, dcInterval = null;

function negotiate() {
    var width = screen.offsetWidth;
    var height = screen.offsetHeight;

    pc.addTransceiver('video', {direction: 'recvonly'});
    pc.addTransceiver('audio', {direction: 'recvonly'});
    return pc.createOffer().then(function(offer) {
        return pc.setLocalDescription(offer);
    }).then(function() {
        // wait for ICE gathering to complete
        return new Promise(function(resolve) {
            if (pc.iceGatheringState === 'complete') {
                resolve();
            } else {
                function checkState() {
                    if (pc.iceGatheringState === 'complete') {
                        pc.removeEventListener('icegatheringstatechange', checkState);
                        resolve();
                    }
                }
                pc.addEventListener('icegatheringstatechange', checkState);
            }
        });
    }).then(function() {
        var offer = pc.localDescription;

        console.log('offer-sdp', offer.sdp);
        return fetch('/offer', {
            body: JSON.stringify({
                sdp: offer.sdp,
                type: offer.type,
                width: width,
                height: height,
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });
    }).then(function(response) {
        console.log(response)
        return response.json();
    }).then(function(answer) {
        console.log('answer-sdp', answer.sdp);
        return pc.setRemoteDescription(answer);
    }).catch(function(e) {
        alert(e);
    });
}

function start() {

    dc = pc.createDataChannel('chat');
    dc.onclose = function() {
        clearInterval(dcInterval);
    };
    dc.onopen = function() {
       
       screen.addEventListener("click", function(event) {
            var positionx = event.offsetX ;// * 800 / 1200;
            var positiony = event.offsetY ;// * 600 / 600;
            var message = "mouse.click(" + positionx + ", " + positiony + ")";
            dc.send(message);
        });

        screen.addEventListener("dblclick", function(event) {
            var message = "mouse.click(" + event.offsetX + ", " + event.offsetY + ", {'clickCount': 2})";
            dc.send(message);
        });

        screen.addEventListener("contextmenu", function(event) {
            event.preventDefault();
            var message = "mouse.click(" + event.offsetX + ", " + event.offsetY + ", {'button':"+"'right'"+"})";
            dc.send(message);
        });

        document.getElementById("goback").addEventListener("click", function(event) {
            dc.send('goBack()');
        });

        document.getElementById("home").addEventListener("click", function(event) {
            home = 'https://start.me/p/b5mnbv/start';
            dc.send('goto("' + home + '")');
        });

        document.getElementById("goforward").addEventListener("click", function(event) {
            dc.send('goForward()');
        });

        document.getElementById("reload").addEventListener("click", function(event) {
            dc.send('reload()');
        });

        document.getElementById("goto").addEventListener("keyup", function(event) {
            if(event.key == "Enter" ) {
                goto();
            }
        });

        document.getElementById("search").addEventListener("click", function(event) {
                goto();
        });

        document.onkeyup = function(event) {
            if(document.activeElement.tagName != "INPUT") {
                dc.send('keyboard.press("' + event.code + '")');
            }
        }

    };
    dc.onmessage = function(evt) {
       
    };

    negotiate();

}

function goto(){
    var goto = document.getElementById("goto").value;
    var pattern = /^((http|https|ftp):\/\/)/;
    if(!pattern.test(goto)){
      dc.send('goto("http://www.google.com/search?hl=en&source=hp&q=' + goto + '&aq=f&oq=&aqi=")');
    }
    dc.send('goto("' + goto + '")');
  }

document.addEventListener('DOMContentLoaded', start);

function stop() {
    // close data channel
    if (dc) {
        dc.close();
    }

    // close peer connection
    setTimeout(function() {
        pc.close();
    }, 500);
}

window.scrollYValue = 0;

window.onwheel = function (e) {
    window.scrollYValue += e.deltaY;
}

setInterval(function() {
    if(window.cloudready == false) {
        window.scrollYValue = 0;
    }
    if(window.scrollYValue != 0 && window.cloudready == true) {
        dc.send('evaluate("() => { window.scrollBy(0, ' + window.scrollYValue + '); }")');
        window.scrollYValue = 0;
    }
}, 1000 / 5);

$("li.dropdown").click(function(){
	if($(this).hasClass("open")) {
		$(this).find(".dropdown-menu").slideUp("fast");
		$(this).removeClass("open");
	}
	else { 
		$(this).find(".dropdown-menu").slideDown("fast");
		$(this).toggleClass("open");
	}
});

// Close dropdown on mouseleave
$("li.dropdown").mouseleave(function(){
	$(this).find(".dropdown-menu").slideUp("fast");
	$(this).removeClass("open");
});

// Navbar toggle
$(".navbar-toggle").click(function(){
	$(".navbar-collapse").toggleClass("collapse").slideToggle("fast");
});


// edges
$("#edges > .btn").click(function(){
	if($(this).is("#rounded")) {
		$(".navbar, .form-control").css("border-radius","8px");
		if($(window).width() > 768) {
			$(".dropdown-menu").css({
				"border-bottom-right-radius":"8px",
				"border-bottom-left-radius":"8px"
			});
		}
	}
	else if($(this).is("#square")) {
		$(".navbar, .form-control").css("border-radius","0");
		$(".dropdown-menu").css({
			"border-bottom-right-radius":"0",
			"border-bottom-left-radius":"0"
		});
	}
})