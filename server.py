import argparse
import asyncio
import json
import logging
import os
import platform
from pyppeteer import launch
from threading import Thread
import traceback
import numpy
import time

from aiohttp import web
from av import VideoFrame
import base64

from aiortc import RTCPeerConnection, RTCSessionDescription
import aiortc.mediastreams
import aiortc.codecs.h264
frame_rate = 10
aiortc.mediastreams.VIDEO_PTIME = 1 / frame_rate
aiortc.codecs.h264.MAX_FRAME_RATE = frame_rate
from aiortc.contrib.media import MediaBlackhole, MediaPlayer, MediaRecorder

from PIL import Image
import io

import traceback
import sys

ROOT = os.path.dirname(__file__)

def load_image(fileName):
    img = Image.open(fileName)
    img.load()
    data = numpy.ascontiguousarray(img, dtype=numpy.uint8)
    data = numpy.ascontiguousarray(data[:,:,:3], dtype=numpy.uint8)
    return data

class CloudBrowserVideoStreamTrack(aiortc.mediastreams.VideoStreamTrack):
    def __init__(self, width, height, screen_queue):
        super().__init__()
        self.screen_queue = screen_queue
        self.black_frame = VideoFrame.from_ndarray(numpy.zeros((height, width, 3), numpy.uint8), format='rgb24')
        self.current_screen = numpy.zeros((height, width, 3), numpy.uint8)

        self.count = 0
        self.t0 = time.time()

    async def recv(self):
        pts, time_base = await self.next_timestamp()
        try:
            try:
                self.current_screen = self.screen_queue.get_nowait()
            except:
                pass
            
            frame = VideoFrame.from_ndarray(self.current_screen, format='rgb24')
            frame.pts = pts
            frame.time_base = time_base

            self.count += 1
            if self.count % 100 == 0:
                print("RECV", self.count / (time.time()-self.t0))
            return frame
        except Exception as e:
            print(e)
            self.black_frame.pts = pts
            self.black_frame.time_base = time_base
            return self.black_frame
async def index(request):
    content = open(os.path.join(ROOT, 'index.html'), 'r').read()
    return web.Response(content_type='text/html', text=content)

async def disconnected(request):
    content = open(os.path.join(ROOT, 'disconnected.html'), 'r', encoding="utf-8").read()
    return web.Response(content_type='text/html', text=content)


async def javascript(request):
    content = open(os.path.join(ROOT, 'client.js'), 'r').read()
    return web.Response(content_type='application/javascript', text=content)

count_sf = 0
t0_sf = time.time()

async def cloud_browser(browser, page, closed, queue, screen_queue):
    
    await page._client.send('Page.startScreencast', {'format': 'jpeg', 'quality': 100, 'everyNthFrame': 1})
    
    @page.on('screencastframe')
    async def on_screencastFrame(event):
        global count_sf
        global t0_sf
        await page._client.send('Page.screencastFrameAck', { 'sessionId': event['sessionId'] })
        screen_queue.put_nowait(load_image(io.BytesIO(base64.b64decode(event['data']))))
        
        count_sf += 1
        if count_sf % 10 == 0:
            print("Screenshot", count_sf / (time.time()-t0_sf))
            count_sf = 0
            t0_sf = time.time()
        
    while True:
        await asyncio.sleep(1.0/50)

        if closed.is_set():
            break

        try:
            message = queue.get_nowait()
            try:
                await eval("page." + message)
            except Exception as e:
                print(e)
                traceback.print_exc()
        except:
            pass
            
    await browser.close()


async def offer(request):
    params = await request.json()
    offer = RTCSessionDescription(
        sdp=params['sdp'],
        type=params['type'])


    pc = RTCPeerConnection()
    pcs.add(pc)
   
    closed = asyncio.Event()
    queue = asyncio.Queue()
    screen_queue = asyncio.Queue()

    browser = await launch({'headless': True, 'args': ['--no-sandbox', '--disable-setuid-sandbox', '--window-size=' + str(params['width']) + ',' + str(params['height'])]}, ignoreDefaultArgs=['--mute-audio'])
    page = await browser.newPage()
    await page._client.send( 'Emulation.clearDeviceMetricsOverride' )
    await page.goto('https://start.me/p/b5mnbv/start')

    @pc.on('datachannel')
    async def on_datachannel(channel):
        #print("Channel opened")

        @channel.on('close')
        def on_close():
            #print("Channel closed")
            closed.set()

        @channel.on('message')
        async def on_message(message):
            await queue.put(message)
            #channel.send(message)
            print(message)
        
        await cloud_browser(browser, page, closed, queue, screen_queue)

    @pc.on('iceconnectionstatechange')
    async def on_iceconnectionstatechange():
        print('ICE connection state is %s' % pc.iceConnectionState)
        if pc.iceConnectionState == 'failed':
            await pc.close()
            pcs.discard(pc)
    
    
    # setup track
    cloud_browser_video_track = CloudBrowserVideoStreamTrack(params['width'], params['height'], screen_queue)

    await pc.setRemoteDescription(offer)
    for t in pc.getTransceivers():
        if t.kind == 'video' and cloud_browser_video_track:
            #pc.addTrack(player.video)
            pc.addTrack(cloud_browser_video_track)
        elif t.kind == 'audio':
            try:
                player = MediaPlayer('default', format='pulse')
                pc.addTrack(player.audio)
            except:
                print("Sound track error")
 
    # send answer
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)

    return web.Response(
        content_type='application/json',
        text=json.dumps({
            'sdp': pc.localDescription.sdp,
            'type': pc.localDescription.type
        }))


pcs = set()


async def on_shutdown(app):
    # close peer connections
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)
    pcs.clear()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='WebRTC audio / video / data-channels demo')
    parser.add_argument('--port', type=int, default=int(os.environ.get('HTTP_PORT', 8080)),
                        help='Port for HTTP server (default: 8080)')
    parser.add_argument('--verbose', '-v', action='count')
    args = parser.parse_args()
    print(args)

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    app = web.Application()
    app.on_shutdown.append(on_shutdown)
    app.router.add_get('/', index)
    app.router.add_get('/disconnected.html', disconnected)
    app.router.add_get('/client.js', javascript)
    app.router.add_post('/offer', offer)
    web.run_app(app, port=args.port)
