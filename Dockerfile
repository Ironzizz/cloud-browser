FROM buildpack-deps:bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
		python3-pip build-essential libssl-dev libffi-dev python3-dev python3-setuptools \
		libavdevice-dev \
		libavfilter-dev \
		libopus-dev \
		libvpx-dev \
		pkg-config \
		libatk-bridge2.0-0 \
		#xvfb \
		gconf-service libasound2 libatk1.0-0 libgtk2.0-0 libnotify-dev libc6 libcairo2 libcups2 \
		libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 \
		libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 \
		libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 \
		libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget \
	&& rm -rf /var/lib/apt/lists/*

#RUN apt-get update && apt-get install -y --no-install-recommends \
#		x11vnc xauth xvfb \
#	&& rm -rf /var/lib/apt/lists/*

#RUN apt-get update && apt-get install -y --no-install-recommends \
#		supervisor xfce4 xfce4-terminal xterm \
#	&& apt-get purge -y pm-utils xscreensaver* \
#	&& rm -rf /var/lib/apt/lists/* 

RUN apt-get update && apt-get install -y \
		pulseaudio socat alsa-utils ffmpeg \
	&& rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y --no-install-recommends \
		xfonts-utils fonts-noto xfonts-intl-asian \
	&& rm -rf /var/lib/apt/lists/*

#COPY fonts/*.* /usr/share/fonts/truetype/

RUN mkfontscale && mkfontdir && fc-cache

RUN pip3 install wheel

#RUN     mkdir ~/.vnc
# Set VNC password to 1234
#RUN     x11vnc -storepasswd 1234 ~/.vnc/passwd

WORKDIR /code
	
COPY requirements.txt /code/requirements.txt

RUN pip3 install -r requirements.txt

COPY pyppeteer /code/pyppeteer
COPY install_chromium.py /code/install_chromium.py
RUN python3 install_chromium.py

COPY . /code/

RUN chmod +x entrypoint.sh

ENTRYPOINT ["/code/entrypoint.sh"]

CMD ["python3", "-u", "server.py"]