#!/bin/bash
#Xvfb :1 -screen 0 1024x768x24 &
#sleep 5
#x11vnc -noxdamage -many -display :1 -rfbauth ~/.vnc/passwd -bg
#nohup x11vnc -forever -usepw -create &
#sleep 2
#xfce4-session &

# Start the pulseaudio server
pulseaudio -D --exit-idle-time=-1

# Load the virtual sink and set it as default
pacmd load-module module-virtual-sink sink_name=v1
pacmd set-default-sink v1

# set the monitor of v1 sink to be the default source
pacmd set-default-source v1.monitor

exec "$@"